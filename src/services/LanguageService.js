import Vue from 'vue'

import VueI18n from 'vue-i18n'

import fr from '@/lang/fr.json'

import languageConstants from '@/lang/language-const.json'

// Multilanguage
Vue.use(VueI18n)

const SUPPORTED_LANGUAGES = languageConstants && languageConstants.hasOwnProperty('supportedLang') ? languageConstants.supportedLang : null
export const DEFAULT_LANGUAGE = navigator.language && SUPPORTED_LANGUAGES.includes(navigator.language.substring(0, 2)) ? navigator.language.substring(0, 2) : (languageConstants && languageConstants.hasOwnProperty('defaultLang') ? languageConstants.defaultLang : 'fr')
const FALLBACK_LANGUAGE = languageConstants && languageConstants.hasOwnProperty('fallbackLang') ? languageConstants.fallbackLang : 'en'

export const i18n = new VueI18n({
  locale: DEFAULT_LANGUAGE, // set locale
  fallbackLocale: FALLBACK_LANGUAGE,
  messages: { fr }, // set locale messages
  silentTranslationWarn: true
})

/**
 * Language service allowing you to setup a translation system
 * @param {*} defaultLangObj
 */
function LanguageService () {
  this.defaultLanguage = DEFAULT_LANGUAGE
  this.fallbackLanguage = FALLBACK_LANGUAGE
  this.supportedLanguages = SUPPORTED_LANGUAGES
}

/**
 * Checks if current lang is the one from the database
 */
LanguageService.prototype.checkLocationLang = function (locationId) {
  let checkedLang = this.getUserSupportedLang(locationId)
  if (i18n.lang !== checkedLang) {
    i18n.lang = checkedLang
  }
}

/**
 * Gets the first supported language that matches the user's
 * @return {String}
 */
LanguageService.prototype.getUserSupportedLang = function (locationId) {
  const userPreferredLang = this.getDefaultLanguage(locationId)

  // Check if user preferred browser lang is supported
  if (this.isLangSupported(userPreferredLang.lang)) {
    return userPreferredLang.lang
  }
  // Check if user preferred lang without the ISO is supported
  if (this.isLangSupported(userPreferredLang.langNoISO)) {
    return userPreferredLang.langNoISO
  }
  return this.defaultLanguage
}

/**
 * Gets the default language from firebase
 */
LanguageService.prototype.getDefaultLanguage = function () {
  return {
    lang: this.defaultLanguage,
    langNoISO: this.defaultLanguage.split('-')[0]
  }
}

LanguageService.prototype.setI18nLocale = function (lang) {
  if (this.isLangSupported(lang)) {
    this.loadLanguage(lang)
  }
}

LanguageService.prototype.setDefaultLanguage = function (lang) {
  this.setI18nLocale(lang)
  document.querySelector('html').setAttribute('lang', lang)
}

/**
 * Returns the users preferred language
 */
LanguageService.prototype.getBrowserLang = function () {
  var lang = window.navigator.language || window.navigator.userLanguage || this.defaultLanguage
  return {
    lang: lang,
    langNoISO: lang.split('-')[0]
  }
}

/**
 * Checks if a lang is supported
 * @param {String} lang
 * @return {boolean}
 */
LanguageService.prototype.isLangSupported = function (lang) {
  return this.supportedLanguages.includes(lang)
}

/**
 * Async loads a translation
 * @param lang
 * @return {Promise<*>|*}
 */
LanguageService.prototype.loadLanguage = function (lang) {
  let newLang = require(`../lang/${lang}.json`)
  i18n.setLocaleMessage(lang, newLang)
  i18n.locale = lang
}

// Export the class
export default new LanguageService()

import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

// Moment.js import
import moment from 'moment'
import 'moment/locale/fr'
import VueMoment from 'vue-moment'

// Frontend framework
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default-dark.css'

// LanguageService and translation plugin import
import LanguageService, { i18n, DEFAULT_LANGUAGE } from './services/LanguageService'

Vue.use(VueMaterial)
Vue.material = {
  ...Vue.material,
  locale: {
    ...Vue.material.locale,
    dateFormat: 'dd/MM/yyyy',
    firstDayOfAWeek: 1
  }
}

// pass custom class to Vue Material
const linkActiveClass = 'my-link-active-class'
Vue.material.router.linkActiveClass = linkActiveClass

// Moment.js
Vue.use(VueMoment, {
  moment
})

LanguageService.setI18nLocale(DEFAULT_LANGUAGE)
// languageService
Vue.prototype.$languageService = LanguageService

// Firebase store relation
Vue.use(Vuex)

Vue.config.productionTip = false
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
